import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Modal, List, Avatar } from 'antd'
import {
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'
import Card from "react-credit-cards"

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'


const Info = styled.div`
  display: flex;
  justify-content: space-between;
`
const Name = styled.div`

`
const Price = styled.div`
	color: #FA4A0C;
`

const HistoryModal = ({ editOrder, setEditOrder }) => {

  return (
		<Modal
      title="Details"
      centered
      visible={editOrder}
      onOk={() => setEditOrder(false)}
      onCancel={() => setEditOrder(false)}
			footer={null}
    >
			 <List
				itemLayout="horizontal"
				dataSource={editOrder?.items}
				footer={<Info><Name>Total:</Name><Price>{editOrder?.totalPrice}</Price></Info>}
				renderItem={item => (
					<List.Item>
						<List.Item.Meta
							avatar={<Avatar src={item?.imageUrl} />}
							title={<Info><Name>{item?.name}</Name><Price>{item?.price}</Price></Info>}
						/>
					</List.Item>
				)}
  		/>

		</Modal>
	)
}

export default HistoryModal
