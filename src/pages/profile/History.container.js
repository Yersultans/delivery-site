import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Col, Breadcrumb, Form, Input, Button, Table } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import Card from 'react-credit-cards'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import HistoryModal from './HistoryModal'

const GET_ORDERS = gql`
  query userOrders($userId: ID) {
    userOrders(userId: $userId) {
      id
      identifier
      items {
        product {
          id
          name
        }
        shop {
          id
          name
        }
        imageUrl
        name
        description
        price
        category
        proteins
        fats
        carbohydrates
        calories
        isVegan
        quantity
        totalPrice
      }
      orderProgresses {
        type
        shop {
          id
        }
        isCompleted
      }
      status
      paymentMethod
      courier {
        id
        username
        phoneNumber
      }
      buyer {
        id
        username
        phoneNumber
      }
      totalPrice
      created_at
    }
  }
`

const History = ({ userId }) => {
  const [orders, setOrders] = React.useState(null)
  const [editOrder, setEditOrder] = React.useState(null)
  const { data, loading, error, networkStatus } = useQuery(GET_ORDERS, {
    fetchPolicy: 'no-cache',
    variables: {
      userId
    }
  })

  React.useEffect(() => {
    if (data && !loading) {
      setOrders(data.userOrders)
    }
  }, [data, loading, error, networkStatus])

  if (loading && !orders) {
    return <Loading />
  }

  const columns = [
    {
      title: 'Number #',
      dataIndex: 'identifier',
      width: '25%'
    },
    {
      title: 'Total Price',
      dataIndex: 'totalPrice',
      width: '25%'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: '25%'
    },
    {
      title: 'Action',
      width: '25%',
      render: (text, item) => (
        <span>
          <Button type="link" onClick={() => setEditOrder(item)}>
            Details
          </Button>
        </span>
      )
    }
  ]

  return (
    <>
      <Table dataSource={orders} columns={columns} rowKey={(item) => item.id} />
      <HistoryModal editOrder={editOrder} setEditOrder={setEditOrder} />
    </>
  )
}

export default History
