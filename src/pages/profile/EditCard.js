import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col, Button, Form, Input } from 'antd'
import {
  PlusCircleOutlined,
	MinusCircleOutlined,
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'
import Card from "react-credit-cards"
import styled from 'styled-components'
import { 
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
  formatFormData 
} from '../shared/utils'
import showConfirm from '../shared/DeleteConfirm'

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
}

const StyledForm = styled(Form)`
  margin-top: 64px;
`

const EditCard = ({ editCard, setEditCard, deleteCard }) => {

  return (
		<Modal
      title="Delete Card"
      centered
      visible={editCard}
      onOk={() => setEditCard(null)}
      onCancel={() => setEditCard(null)}
			footer={[
				<Button 
					danger  
					size="large" 
					onClick={() => {
						showConfirm(() => {
							deleteCard(editCard.id)
						})
				}}>
            Delete
        </Button>
			]}
    >
			<Card
        number={editCard?.number}
        name={editCard?.name}
        expiry={editCard?.expiry}
        cvc={editCard?.cvc}
      />
    </Modal>
  )
}

EditCard.propTypes = {
  product: PropTypes.objectOf(PropTypes.any).isRequired,
  setOpen: PropTypes.func.isRequired
}

export default EditCard
