import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Col, Breadcrumb, Form, Input, Button, Tabs, Avatar  } from 'antd'
import {
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import { removeAnnoyingHeader } from '../../utils/apollo'
import ImageUpload from '../shared/ImageUpload'

const { TabPane } = Tabs;

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const layout = {
  labelCol: {
    span: 4
  },
  wrapperCol: {
    span: 8
  }
}
const tailLayout = {
  wrapperCol: {
    offset: 2,
    span: 8
  }
}

const UPDATE_USER = gql`
  mutation updateUser($id: ID!, $input: UserInput) {
    updateUser(id: $id, input: $input) {
      id
      firstname
      lastname
      phoneNumber
      username
      role
      avatarUrl
    }
  }
`

const EditUser = ({ user }) => {
  const { fetchUser } = useAuth()
  const [avatarUrl, setAvatarUrl] = React.useState(user?.avatarUrl)
	const [updateUser] = useMutation(UPDATE_USER,{
    onCompleted() {
      fetchUser()
			toast.success('Information successfully updated')
    }
  })
  
  const handleUpdateClick = (values) => {
    delete values.avatarUrl
    updateUser({ variables: { id: user?.id, input: { ...values, avatarUrl} } })
  }
  return (
		<Form
        name="basic"
				{...layout}
        initialValues={{
          remember: true
        }}
        onFinish={handleUpdateClick}
      >
        <Form.Item
          labelAlign="left"
          label="Firstname"
          name="firstname"
          initialValue={user?.firstname}
        >
          <Input placeholder="Firstname"  />
        </Form.Item>
				<Form.Item
          labelAlign="left"
          label="Lastname"
          name="lastname"
          initialValue={user?.lastname}
        >
          <Input placeholder="Lastname"  />
        </Form.Item>
				<Form.Item
          labelAlign="left"
          label="PhoneNumber"
          name="phoneNumber"
          initialValue={user?.phoneNumber}
        >
          <Input placeholder="PhoneNumber"  />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          key="avatarUrl"
          label="Avatar"
          name="avatarUrl"
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setAvatarUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={avatarUrl ||  user?.avatarUrl} />
            </ImageUploadContainer>
          </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" size="large">
            Update
          </Button>
        </Form.Item>
      </Form>
	)
}

export default EditUser
