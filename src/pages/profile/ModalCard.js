import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col, Button, Form, Input } from 'antd'
import {
  PlusCircleOutlined,
	MinusCircleOutlined,
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'
import Card from "react-credit-cards"
import { 
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
  formatFormData 
} from '../shared/utils'
import styled from 'styled-components'

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
}

const StyledForm = styled(Form)`
  margin-top: 64px;
`

const CardModal = ({ visible, setVisible, addCard }) => {
	const [name, setName] = React.useState('')
	const [number, setNumber] = React.useState('')
	const [expiry, setExpiry] = React.useState('')
	const [cvc, setCvc] = React.useState('')
  const [focused, setFocused] = React.useState('')

  const handleInputFocus = ({ target }) => {
    setFocused(target.name)
  }

  const handleCallback = ({ issuer }, isValid) => {
    if (isValid) {
      console.log('asdasd')
    }
  }

  const handleInputChange = ({ target }) => {
    if (target.name === "number") {
      
      target.value = formatCreditCardNumber(target.value)
      setNumber(target.value)
    } else if (target.name === "expiry") {
      target.value = formatExpirationDate(target.value)
      setExpiry(target.value)
    } else if (target.name === "cvc") {
      target.value = formatCVC(target.value)
      setCvc(target.value)
    }else if (target.name === "name"){
      setName(target.value)
    }
  };

  return (
		<Modal
      title="Add Card"
      centered
      visible={visible}
      onOk={() => setVisible(false)}
      onCancel={() => setVisible(false)}
			footer={null}
    >
			<Card
        number={number}
        name={name}
        expiry={expiry}
        cvc={cvc}
        focused={focused}
        callback={handleCallback}
      />
      <StyledForm
        layout="vertical"
        onFinish={() => {
          addCard({
            number: number,
            name: name,
            expDate: expiry,
            cvv: cvc,
            type: number.charAt(0) === '4' ? 'visa' : 'mastercard'
          })
          setName('')
          setCvc('')
          setNumber('')
          setExpiry('')
          setFocused('')
          setVisible(false)
        }}
      >
        <Form.Item
          label="Number"
          name="number"
          rules={[{ required: true } ]}
        >
          <Input 
            placeholder="Number"
            name="number"
            onChange={handleInputChange} 
            onFocus={handleInputFocus}
            />
        </Form.Item>
				<Form.Item
          label="Name"
          name="name"
          rules={[{ required: true }]}
        >
          <Input 
            placeholder="Name"  
            name="name"
            onChange={handleInputChange}
            onFocus={handleInputFocus}
            />
        </Form.Item>
				<Form.Item
          name="expiry"
          label="Expiry"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}
        >
          <Input placeholder=""  name="expiry" type='tel' onChange={handleInputChange}/>
        </Form.Item>
        <Form.Item
          name="cvc"
          label="CVC"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}
        >
          <Input placeholder="" name="cvc"  onFocus={handleInputFocus} onChange={handleInputChange}/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" size="large">
            Add
          </Button>
        </Form.Item>
      </StyledForm>
    </Modal>
  )
}

CardModal.propTypes = {
  product: PropTypes.objectOf(PropTypes.any).isRequired,
  setOpen: PropTypes.func.isRequired
}

export default CardModal
