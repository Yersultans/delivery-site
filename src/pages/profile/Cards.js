import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Col, Breadcrumb, Form, Input, Button, Tabs  } from 'antd'
import {
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'
import Card from "react-credit-cards"

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import { removeAnnoyingHeader } from '../../utils/apollo'
import ModalCard from './ModalCard'
import EditCard from './EditCard'

const { TabPane } = Tabs;

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const layout = {
  labelCol: {
    span: 2
  },
  wrapperCol: {
    span: 8
  }
}
const tailLayout = {
  wrapperCol: {
    offset: 2,
    span: 8
  }
}

const AllCards = styled.div`
	display: flex;
  margin-top: 30px;
  flex-wrap: wrap;
  /* justify-content: space-between; */
`

const CardContainer = styled.div`
	margin-right: 24px;
	margin-bottom: 24px;
	border-radius: 14px;
	transition: box-shadow 0.3s, border-color 0.3s;
	cursor: pointer;
  &:hover {
    border-color: transparent;
    box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }
`

const GET_CARDS = gql`
  query userCards($userId: ID) {
    userCards(userId: $userId) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`

const ADD_CARD = gql`
  mutation addCard($input: CardInput) {
    addCard(input: $input) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`
const DELETE_CARD = gql`
  mutation deleteCard($id: ID!) {
    deleteCard(id: $id)
  }
`

const Cards = ({ userId }) => {

	const [modalVisible, setModalVisible] = React.useState(false)
	const [userCards, setUserCards] = React.useState(null)
	const [editCard, setEditCard] = React.useState(null)

	const { data, loading, error, refetch } = useQuery(GET_CARDS, {
    fetchPolicy: 'no-cache',
    variables: {
      userId
    }
  })
	const [addCard] = useMutation(ADD_CARD, {
    onCompleted() {
      refetch()
			toast.success('Card successfully added')
    }
  })
	const [deleteCard] = useMutation(DELETE_CARD, {
    onCompleted() {
      refetch()
			toast.success('Card successfully removed')
    }
  })

	React.useEffect(() => {
    if (data) {
      setUserCards(data.userCards)
    }
  }, [data, loading, error])

	const handleAdd = values => {
    addCard({
      variables: {
        input: { ...values, user: userId }
      },
      errorPolicy: 'all'
    })
    refetch()
  }
	const handleDelete = id => {
    deleteCard({
      variables: {
        id
      }
    })
    setUserCards(userCards.filter(card => card.id !== id))
    refetch()
  }
	if (loading && !userCards) {
    return <Loading />
  }

  return (
		<>
		<Button type='primary' onClick={() => setModalVisible(true)}>Add Card</Button>
			<AllCards>
				{userCards && userCards.map(userCard => (
					<CardContainer 
						onClick={() => setEditCard(userCard)}
					>
						<Card
							name={userCard?.name}
							number={userCard?.number}
							expiry={userCard?.expDate}
							cvc={userCard?.cvv}
						/>
					</CardContainer>
				))}
			</AllCards>
			<ModalCard visible={modalVisible} setVisible={setModalVisible} addCard={handleAdd}/>
			<EditCard editCard={editCard} setEditCard={setEditCard} deleteCard={handleDelete}/>
		</>
	)
}

export default Cards
