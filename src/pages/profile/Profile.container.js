import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import Profile from './Profile'
import { removeAnnoyingHeader } from '../../utils/apollo'




const ProfileContainer = () => {
	const { user } = useAuth()
		return <Profile
			user={user}
		/>
}

export default WithMainLayout(ProfileContainer)
