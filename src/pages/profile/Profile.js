import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Col, Breadcrumb, Form, Input, Button, Tabs  } from 'antd'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import EditUser from './EditUser'
import Cards from './Cards'
import History from './History.container'
import { removeAnnoyingHeader } from '../../utils/apollo'

const { TabPane } = Tabs;

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const Profile = ({ user }) => {

  
	const callback = (key) => {
		console.log(key);
	}
	
  return (
		<>
			<StyledBreadcrumb>
					<Breadcrumb.Item>
						<Link to="/home">Home</Link>
					</Breadcrumb.Item>
					<Breadcrumb.Item>Profile</Breadcrumb.Item>
				</StyledBreadcrumb>
				<Tabs onChange={callback} type="card">
					<TabPane tab="Profile" key="profile">
						{user && (
							<EditUser user={user}/>
						)}
						
					</TabPane>
					<TabPane tab="History" key="history">
						<History userId={user?.id} />
					</TabPane>
					<TabPane tab="Cards" key="cards">
						<Cards userId={user?.id}/>
					</TabPane>
				</Tabs>
		</>
	)
}

export default Profile
