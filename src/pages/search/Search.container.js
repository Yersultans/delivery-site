import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { Row, Col, Breadcrumb, Form, Input, Button } from 'antd'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import { removeAnnoyingHeader } from '../../utils/apollo'
import Loading from '../../pages/shared/Loading'
import ProductModal from '../home/ProductModal'

const GET_PPODUCTS = gql`
  query getProducts($text: String) {
    findProducts(text: $text) {
      id
      name
      description
      imageUrl
      category
      proteins
      fats
      carbohydrates
      calories
      isVegan
    }
  }
`
const GET_SHOP_PRODUCTS = gql`
  query getShopProducts($productId: ID) {
    shopProductsByProduct(productId: $productId) {
      id
      price
      shop {
        id
        name
        imageUrl
      }
    }
  }
`

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const StyledCol = styled(Col)`
  padding: 12px;
  
`
const StyledImg = styled.img`
  border-radius: 8px;
  width: -webkit-fill-available;
  height: 160px;
`

const ProductName = styled.div`
  padding: 8px 0;
  text-align: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`

const ProductContainer = styled.div`
  background-color: #fff1e6;
  padding: 12px;
  border-radius: 8px;
  cursor: pointer;
  transition: box-shadow 0.3s, border-color 0.3s;
  &:hover {
    border-color: transparent;
    box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }
`

const findGetParameter = params => {
	var result = null,
		tmp = []
	window.location.search
		.substr(1)
		.split('&')
		.forEach(item => {
			tmp = item.split('=')
			if (tmp[0] === params) result = decodeURIComponent(tmp[1])
		})
	return result
}

const SearchContainer = ({ route }) => {
  const { user, sum, items, addItem, deleteItem, fetchUser } = useAuth()
	const [products, setProducts] = React.useState([])
	const text = findGetParameter('text')
	const [product, setProduct] = React.useState(null)
  const [shopProducts, setShopProducts] = React.useState([])
	const [selectProduct, setSelectProduct] = React.useState(null)

	const [getProducts, { loading, error, data }] = useLazyQuery(GET_PPODUCTS)
	const [
    getShopProducts,
    {
      data: dataShopProduct,
      loading: loadingShopProduct,
      error: errorShopProduct
    }
  ] = useLazyQuery(GET_SHOP_PRODUCTS, {
    fetchPolicy: 'no-cache'
  })
  
	React.useEffect(() => {
		if(text){
			getProducts({ variables: { text: text } })
		}
	}, [text])

	React.useEffect(() => {
    if (data) {
      setProducts(data.findProducts)
    }
  }, [data, loading, error])

	React.useEffect(() => {
    if (dataShopProduct && !loadingShopProduct) {
      setShopProducts(dataShopProduct.shopProductsByProduct)
    }
  }, [dataShopProduct, loadingShopProduct, errorShopProduct])

	React.useEffect(() => {
    if (selectProduct) {
      getShopProducts({
        variables: {
          productId: selectProduct.id
        }
      })
    }
  }, [selectProduct])

	if (loading) {
    return <Loading />
  }

  return (
		<>
			<StyledBreadcrumb>
				<Breadcrumb.Item>
					<Link to="/home">Home</Link>
				</Breadcrumb.Item>
				<Breadcrumb.Item>Search</Breadcrumb.Item>
			</StyledBreadcrumb>
			<Row gutter={24}>
        {products &&
          products.map((product, index) => (
            <>
            <StyledCol onClick={() => {
              setSelectProduct(product)
            }}
            className="gutter-row" span={6} >
            	<ProductContainer>
              	<StyledImg src={product?.imageUrl} />
                <ProductName>{product?.name}</ProductName>
              </ProductContainer>
            </StyledCol>
            </>
        ))}
      </Row>
			{selectProduct && (
        <ProductModal
          items={items}
          addItem={addItem}
          deleteItem={deleteItem}
          shopProducts={shopProducts}
          product={selectProduct}
          setProduct={setSelectProduct}
        />
      )}
		</>
	)
}

export default WithMainLayout(SearchContainer)
