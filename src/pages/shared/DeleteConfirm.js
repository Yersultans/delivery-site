import { Modal } from 'antd'

const confirmModal = Modal.confirm

const showConfirm = func => {
  confirmModal({
    title: 'Are you sure you want to delete this item?',
    content: 'This item will be deleted',
    okType: 'primary',
    onOk: () => func(),
    // eslint-disable-next-line no-console
    onCancel: () => console.log('Cancel'),
    cancelText: 'Cancel',
    okText: 'Delete'
  })
}

export default showConfirm
