import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Layout, 
  Menu,
  Breadcrumb,
  Row, 
  Col,
  Carousel,
  Card 
} from 'antd'
import { PlusOutlined, SearchOutlined, RightOutlined, LeftOutlined, MinusCircleOutlined, PlusCircleOutlined   } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import { ScrollMenu, VisibilityContext } from "react-horizontal-scrolling-menu"
import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const FormItem = Form.Item
const { Meta } = Card

const { Header, Sider, Content, Footer } = Layout

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
  border-radius: 8px;
`

const StyledHeader = styled(Header)`
  background: #fff;
  border-bottom: 1px solid #d9d9d9;
  padding: 0px 24px;
  line-height: 32px;
`

const StyledMenu = styled(Menu)`
  height: 100%;
  padding-top: 16px;
`
const StyledLayout = styled(Layout)`
  padding: 0 24px 24px;
  background: #fff;
`

const StyledContent = styled(Content)`
  padding: 24px;
  margin: 0px;
  min-height: 280px;
`

const ProductContainer = styled.div`
  background-color: #fff1e6;
  padding: 12px;
  border-radius: 8px;
  cursor: pointer;
  transition: box-shadow 0.3s, border-color 0.3s;
  &:hover {
    border-color: transparent;
    box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }
`

const StyledCol = styled(Col)`
  padding: 12px;
  
`
const ProductName = styled.div`
  padding: 8px 0;
  text-align: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`

const StyledImg = styled.img`
  border-radius: 8px;
  width: -webkit-fill-available;
  height: 160px;
`
const CarouselStyledImg = styled.img`
  min-height: 360px;
  background-color: #fff;
  /* border-radius: 18px; */
  
`

const CarouselContainer = styled.div`
  
`

const StyledCarousel = styled(Carousel)`
  background-color: #fff;
`
const Container = styled.div`
  padding-top: 64px;
  background: #fff;
`

const ScrollContainer = styled.div`
  display: flex;
  overflow-x: scroll;
  padding: 16px 8px;
`

const CardContainer = styled.div`
  margin: 0 16px;
  cursor: pointer;
  width: 240;
  transition: box-shadow 0.3s, border-color 0.3s;
  border-radius: 8px;
  &:hover {
    border-color: transparent;
    box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }
  border-style: solid;
  border-width: 1px;
  border-color: #f0f0f0;
`

const StyledCardImg = styled.img`
  width: 100%;
  border-radius: 8px 8px 0 0;
`

const ImageDiv = styled.div`
  width: 240px;
  height: 240px;
`

const CardInfo = styled.div`
  padding: 24px;
`

const CardName = styled.div`
  overflow: hidden;
  color: rgba(0, 0, 0, 0.85);
  font-weight: 500;
  font-size: 16px;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin-bottom: 8px;
  text-align: center;
`

const CardPrice = styled.div`
  overflow: hidden; 
  color: #FA4A0C;
  font-weight: 500;
  font-size: 14px;
  white-space: nowrap;
  text-overflow: ellipsis;
  text-align: center;
  line-height: 32px;
`

const ShopProductQuantity = styled.div`
	display: flex;
  justify-content: center;
	align-items: center;
	height: 32px;
`

const ShopProductNumber = styled.div`
		font-style: normal;
		font-weight: 600;
		font-size: 16px;
		line-height: 20px;
		color: #FA4A0C;
		margin: 0 8px;
`

const includesCategory = ({ category, categories }) => {
  return categories.includes(category)
}

const includesProduct = ({ items, productId, shopId }) => {
  let isIncludes = false
  items.forEach(item => {
    if (item.id === productId && item.shop.id === shopId) {
      isIncludes = true
    }
  })
  return isIncludes
}

const Home = ({ 
  products, 
  categories,
  currentTab, 
  setCurrentTab, 
  selectProducts, 
  setSelectProducts, 
  selectProduct, 
  setSelectProduct,
  discountShopProducts,
  sum, 
  items, 
  addItem, 
  deleteItem,
  user
}) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [imageUrl, setImageUrl] = React.useState(null)

  const handleTabChange = activeKey => {
    setCurrentTab(activeKey)
  }

  function onChange(a, b, c) {
    console.log(a, b, c);
  }
  

  return (
    <Layout>
      <div
      style={{
        backgroundColor: '#fff',
        paddingBottom: 32
      }}
      >
       <Carousel autoplay>
          <CarouselContainer >
            <CarouselStyledImg src="https://arbuz.kz/image/f/e25133d6-c94e-400b-a9f0-e0ce6e45975e-325_s_1_newsize_jpg.jpg?w=1450&h=:h&c=1650613950" />
          </CarouselContainer>
          <CarouselContainer >
            <CarouselStyledImg src="https://arbuz.kz/image/f/e25133d6-c94e-400b-a9f0-e0ce6e45975e-325_s_1_newsize_jpg.jpg?w=1450&h=:h&c=1650613950" />
          </CarouselContainer>
          <CarouselContainer >
            <CarouselStyledImg src="https://arbuz.kz/image/f/e25133d6-c94e-400b-a9f0-e0ce6e45975e-325_s_1_newsize_jpg.jpg?w=1450&h=:h&c=1650613950" />
          </CarouselContainer>
          <CarouselContainer>
            <CarouselStyledImg src="https://arbuz.kz/image/f/e25133d6-c94e-400b-a9f0-e0ce6e45975e-325_s_1_newsize_jpg.jpg?w=1450&h=:h&c=1650613950" />
          </CarouselContainer>
        </Carousel>
        </div>
        <Container>
        <StyledHeader>
        <h2>Discount</h2>
      </StyledHeader>
      <StyledLayout>
        <ScrollContainer>
      {discountShopProducts.length > 0 && discountShopProducts.map((item) => {
        const productInBacket = items.find(
          itemData => itemData?.id === item?.product?.id && item?.shop?.id === itemData?.shop?.id
        )
        return(
            <CardContainer
              style={{ width: 242
               }}
            >
              <ImageDiv>
                <StyledCardImg src={item?.product?.imageUrl} />
              </ImageDiv>
              <CardInfo>
                <CardName>{item?.product?.name}({item?.shop?.name})</CardName>
                {includesProduct({
        			items,
       				productId: item?.product?.id,
        			shopId: item?.shop?.id
      			}) ? (
              <ShopProductQuantity>
								<MinusCircleOutlined onClick={() => 
								deleteItem(item?.product?.id, item?.shop?.id)
							} style={{
									color: '#FA4A0C'
								}}/>
								<ShopProductNumber>{productInBacket?.quantity}</ShopProductNumber>
								<PlusCircleOutlined 
								onClick={() => 
									addItem({
										...item?.product,
										price: item?.price,
										shop: item?.shop
								})	
								}
								style={{
									color: '#FA4A0C'
								}}/>
							</ShopProductQuantity>
            ) : (
            <CardPrice 
                onClick={() => 
                  addItem({
                    ...item?.product,
                    price: item?.price,
                    shop: item?.shop
                })}
              >{item?.price}₸</CardPrice>
             
            )}
              </CardInfo>
            </CardContainer>
        )
      }
            )}
            </ScrollContainer>
      </StyledLayout>
      </Container>
      <Container>
       <StyledHeader>
        <h2>Products</h2>
      </StyledHeader>
      <Layout>
        <Sider>
          <StyledMenu mode="inline" defaultSelectedKeys={[currentTab]}>
            { categories && categories.map(category => (
              <Menu.Item
                key={category?.identifier}
                onClick={() => {
                  handleTabChange(category?.identifier)
                  setSelectProducts(products.filter(product => product?.category === category?.identifier))
                }}
               >
                {category?.name}
              </Menu.Item>
            ))
            }
            <Menu.Item
                key="other"
                onClick={() => {
                  handleTabChange('other')
                  setSelectProducts(
                    products.filter(
                      product =>
                        !includesCategory({
                          category: product.category,
                          categories: categories.map(
                            category => category.identifier
                          )
                        })
                    )
                  )
                }}
               >
                Other
            </Menu.Item>
          </StyledMenu>
        </Sider>
        <StyledLayout>
          <StyledContent className="site-layout-background">
          <Row gutter={24}>
            {selectProducts &&
              selectProducts.map((product, index) => (
                <>
                <StyledCol onClick={() => {
                  setSelectProduct(product)
                }}
                className="gutter-row" span={8} >
                  <ProductContainer>
                    <StyledImg src={product?.imageUrl} />
                    <ProductName>{product?.name}</ProductName>
                  </ProductContainer>
                 </StyledCol>
            </>
            ))}
          </Row>
          </StyledContent>
        </StyledLayout>
      </Layout>
      </Container>
    </Layout>
  )
}

Home.propTypes = {
  shops: PropTypes.arrayOf(PropTypes.object).isRequired,
  categories: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Home
