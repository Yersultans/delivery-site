import React from 'react'
import moment from 'moment'
import { toast } from 'react-toastify'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { Comment, List, Tooltip, Form, Input, Button } from 'antd'

const { TextArea } = Input

const GET_DATA = gql`
  query getData($productId: ID) {
    productCommentsByProduct(productId: $productId) {
      id
			text
  		rating
			user{
				id
				firstname
				lastname
				avatarUrl
			}
  		created_at
  		updated_at
    }
  }
`
const ADD_PRODUCT_COMMENT = gql`
  mutation addProductComment($input: ProductCommentInput) {
    addProductComment(input: $input) {
      id
    }
  }
`

const ProductCommentsContainer = ({ productId, userId }) => {
	const [productComments, setProductComments] = React.useState([])

	const [getData, { data, loading, error, refetch }] = useLazyQuery(GET_DATA)

	const [addProductComment] = useMutation(ADD_PRODUCT_COMMENT, {
    onCompleted() {
			refetch()
			toast.success('Review added')
    }
  })
	React.useEffect(() => {
    if (productId) {
      getData({ variables: { productId: productId }})
    } 
  }, [productId])

	React.useEffect(() => {
    if (data && !loading && data.productCommentsByProduct) {
      setProductComments(data.productCommentsByProduct)
    } else if (error && !loading) {
      console.log('error', error)
    }
  }, [data, loading, error])

	const handleSubmit = values => {
		console.log('values', values)
		addProductComment({
			variables: {
				input: {
					text: values.text,
					product: productId,
					user: userId
				}
			}
		})
  }

	return (
		<>
			<Form
				name="basic"
				initialValues={{
					remember: true
				}}
				onFinish={handleSubmit}
			>
				<Form.Item
					name="text"
					rules={[
						{
							required: true,
							message: 'Please input your comment!'
						}
					]}
				>
      		<TextArea rows={4} />
    		</Form.Item>	
				<Form.Item>
					<Button htmlType="submit" type="primary">
						Add Comment
					</Button>
    		</Form.Item>
			</Form>
		{productComments && productComments.map(item => (
				<Comment
					author={`${item?.user?.firstname} ${item?.user?.lastname}`}
					avatar={item?.user?.avatarUrl || 'https://joeschmoe.io/api/v1/random'}
					content={item?.text}
					datetime={() => (
						<Tooltip title={moment(item.create_at).format('YYYY-MM-DD')}>
        			<span>{moment(item.create_at).format('YYYY-MM-DD')}</span>
      			</Tooltip>
					)}
				/>
			))
		}
		</>
	)
	
}

export default ProductCommentsContainer