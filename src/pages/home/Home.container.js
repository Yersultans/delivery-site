import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import Home from './Home.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import ProductModal from './ProductModal'
import LoginModal from './LoginModal'
import { removeAnnoyingHeader } from '../../utils/apollo'

const GET_DATA = gql`
  query getData {
    products {
      id
      name
      description
      imageUrl
      category
      proteins
      fats
      carbohydrates
      calories
      isVegan
    }
    activeCategories {
      id
      name
      identifier
    }
    discountShopProducts{
      id
      price
      shop {
        id
        name
        imageUrl
      }
      product{
        id
        name
        imageUrl
      }
    }
  }
`

const GET_SHOP_PRODUCTS = gql`
  query getShopProducts($productId: ID) {
    shopProductsByProduct(productId: $productId) {
      id
      price
      shop {
        id
        name
        imageUrl
      }
    }
  }
`



const HomeContainer = () => {
  const { user, sum, items, addItem, deleteItem, fetchUser } = useAuth()
  const { showLoading, hideLoading } = useLoading()
  const [products, setProducts] = React.useState([])
  const [categories, setCategories] = React.useState([])
  const [shopProducts, setShopProducts] = React.useState([])
  const [selectProducts, setSelectProducts] = React.useState([])
  const [selectProduct, setSelectProduct] = React.useState(null)
  const [currentTab, setCurrentTab] = React.useState(null)
  const [discountShopProducts, setDiscountShopProducts] = React.useState([])

  

  const { data, loading, error } = useQuery(GET_DATA)

  const [
    getShopProducts,
    {
      data: dataShopProduct,
      loading: loadingShopProduct,
      error: errorShopProduct
    }
  ] = useLazyQuery(GET_SHOP_PRODUCTS, {
    fetchPolicy: 'no-cache'
  })

  React.useEffect(() => {
    if (data && !loading && data.activeCategories) {
      setProducts(data.products)
      setCategories(data.activeCategories)
      setCurrentTab(data.activeCategories[0]?.identifier)
      setSelectProducts(data.products.filter(product => product?.category === data.activeCategories[0].identifier))
      setDiscountShopProducts(data.discountShopProducts)
    } else if (error && !loading) {
      console.log('error', error)
    }
  }, [data, loading, error])

  React.useEffect(() => {
    if (dataShopProduct && !loadingShopProduct) {
      setShopProducts(dataShopProduct.shopProductsByProduct)
    } else if (errorShopProduct && !loadingShopProduct) {
      console.log('error', errorShopProduct)
    }
  }, [dataShopProduct, loadingShopProduct, errorShopProduct])

  React.useEffect(() => {
    if (selectProduct) {
      getShopProducts({
        variables: {
          productId: selectProduct.id
        }
      })
    }
  }, [selectProduct])

  

  if (loading && !currentTab) {
    return <Loading />
  }

  

  return (
        <>
        { 
          currentTab && (
              <Home
              products={products}
              categories={categories}
              currentTab={currentTab}
              setCurrentTab={setCurrentTab}
              selectProducts={selectProducts}
              setSelectProducts={setSelectProducts}
              selectProduct={selectProduct} 
              setSelectProduct={setSelectProduct}
              getShopProducts={getShopProducts}
              discountShopProducts={discountShopProducts}
              sum={sum} 
              items={items} 
              addItem={addItem} 
              deleteItem={deleteItem}
              user={user}
            />
          )
        }
         
          {selectProduct && (
            <ProductModal
              user={user}
              items={items}
              addItem={addItem}
              deleteItem={deleteItem}
              shopProducts={shopProducts}
              product={selectProduct}
              setProduct={setSelectProduct}
            />
          )}
          
        </>

  )
}

export default WithMainLayout(HomeContainer)
