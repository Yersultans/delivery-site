import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col, Button, Tabs } from 'antd'
import {
  PlusCircleOutlined,
	MinusCircleOutlined
} from '@ant-design/icons'
import styled from 'styled-components'
import ProductCommentsContainer from './ProductComments.container'

const { TabPane } = Tabs

const StyledCol = styled(Col)`
  padding: 12px;
`

const StyledImg = styled.img`
  border-radius: 8px;
  width: -webkit-fill-available;
  height: 320px;
`

const Title = styled.div`
	font-style: normal;
	font-weight: 600;
	font-size: 16px;
	line-height: 20px;
	margin-bottom: 8px;
`

const ShopTitle = styled.div`
	font-style: normal;
	font-weight: 600;
	font-size: 20px;
	line-height: 24px;
	margin-top: 16px;
	margin-bottom: 16px;
`

const ShopProductContainer = styled.div`
	display: flex;
	justify-content: space-between;
	margin-bottom: 8px;
	align-items: center;
`

const ShopProductName = styled.div`
	font-style: normal;
	font-weight: 600;
	font-size: 16px;
	line-height: 20px;
	
`

const ShopProductPrice = styled(Button)`
	font-style: normal;
	font-weight: 600;
	font-size: 16px;
	line-height: 20px;
	color: #FA4A0C;
	width: 96px;
`

const ShopProductNumber = styled.div`
		font-style: normal;
		font-weight: 600;
		font-size: 16px;
		line-height: 20px;
		color: #FA4A0C;
		margin: 0 8px;
`

const ShopProductQuantity = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	height: 32px;
	width: 96px;
`
const includesProduct = ({ items, productId, shopId }) => {
  let isIncludes = false
  items.forEach(item => {
    if (item.id === productId && item.shop.id === shopId) {
      isIncludes = true
    }
  })
  return isIncludes
}

const ProductModal = ({ user, shopProducts, items, deleteItem, addItem, product, setProduct }) => {
	
  return (
		<Modal
      title={product?.name}
      centered
      visible={product}
      onOk={() => setProduct(null)}
      onCancel={() => setProduct(null)}
      width={1000}
			footer={null}
    >
		<Row>
      <StyledCol span={12}>
				<StyledImg src={product?.imageUrl} />
			</StyledCol>
      <StyledCol span={12}>
				<Tabs defaultActiveKey="1">
					<TabPane tab="Info" key="1">
					<Row>
					<Col span={12}>
					<Title>Name:</Title>
					</Col>
					<Col span={12}>
						<Title>{product?.name}</Title>
					</Col>
				</Row>
				<Row>
					<Col span={12}>
					<Title>Desc:</Title>
					</Col>
					<Col span={12}>
						<Title>{product?.description}</Title>
					</Col>
				</Row>
				<ShopTitle>Shops: </ShopTitle>
				{shopProducts && shopProducts.map(shopProduct => {
					const productInBacket = items.find(
						item => item?.id === product?.id && shopProduct?.shop?.id === item?.shop?.id
					)
					return (
					<ShopProductContainer>
						<ShopProductName>{shopProduct?.shop?.name}</ShopProductName>
						{includesProduct({
        			items,
       				productId: product?.id,
        			shopId: shopProduct?.shop?.id
      			}) ? (
							<ShopProductQuantity>
								<MinusCircleOutlined onClick={() => 
								deleteItem(product.id, shopProduct?.shop?.id)
							} style={{
									color: '#FA4A0C'
								}}/>
								<ShopProductNumber>{productInBacket?.quantity}</ShopProductNumber>
								<PlusCircleOutlined 
								onClick={() => 
									addItem({
										...product,
										price: shopProduct?.price,
										shop: shopProduct?.shop
								})	
								}
								style={{
									color: '#FA4A0C'
								}}/>
							</ShopProductQuantity>
						) : (
							<ShopProductPrice onClick={() => 
								addItem({
									...product,
									price: shopProduct?.price,
									shop: shopProduct?.shop
              })}>{shopProduct?.price}₸</ShopProductPrice>
						)}
						
					</ShopProductContainer>
					)
					
			})}
					</TabPane>
					<TabPane tab="Comment" disabled={!user} key="2">
						<ProductCommentsContainer productId={product?.id} userId={user?.id}/>
					</TabPane>
				</Tabs>
				
				
			</StyledCol>
    </Row>
    </Modal>
  )
}

ProductModal.propTypes = {
  product: PropTypes.objectOf(PropTypes.any).isRequired,
  setOpen: PropTypes.func.isRequired
}

export default ProductModal
