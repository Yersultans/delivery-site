import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col, Button, Form, Input } from 'antd'
import {
  PlusCircleOutlined,
	MinusCircleOutlined,
	UserOutlined,
	LockOutlined
} from '@ant-design/icons'
import styled from 'styled-components'

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
}

const LoginModal = ({ handleSubmit, visible, setVisible }) => {
	
  return (
		<Modal
      title="Sing In"
      centered
      visible={visible}
      onOk={() => setVisible(false)}
      onCancel={() => setVisible(false)}
			footer={null}
    >
			<Form
        name="basic"
				{...layout}
        initialValues={{
          remember: true
        }}
        onFinish={handleSubmit}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!'
            }
          ]}
        >
          <Input placeholder="Username" prefix={<UserOutlined />} />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!'
            }
          ]}
        >
          <Input.Password
            placeholder="Password"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" size="large">
            Sign In
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
}

LoginModal.propTypes = {
  product: PropTypes.objectOf(PropTypes.any).isRequired,
  setOpen: PropTypes.func.isRequired
}

export default LoginModal
