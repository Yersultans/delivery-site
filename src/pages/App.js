import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer } from 'react-toastify'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  concat
} from '@apollo/client'
import 'typeface-roboto'

import './App.css'
import './main.css'
import Login from './auth/Login'
import Home from './home/Home.container'
import Basket from './basket/Basket.container'
import Profile from './profile/Profile.container'
import Search from './search/Search.container'

import PublicPage from './PublicPage'

import withHelmet from '../hocs/withHelmet'
import { ProvideAuth } from '../context/useAuth'
import { ProvideLoading } from '../context/useLoading'
import LoadingDialog from '../context/LoadingDialog'
import PrivateRoute from '../hocs/PrivateRoute'

const httpLink = new HttpLink({
  uri: 'https://delivery-backend-staging.herokuapp.com/graphql'
})

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('token')
        ? `Bearer ${localStorage.getItem('token')}`
        : null
    }
  })
  console.log('asd')
  return forward(operation)
})

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink)
})

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <ProvideAuth>
        <ProvideLoading>
          <BrowserRouter>
            <ToastContainer />
            <LoadingDialog />
            <Route exact path="/public" component={PublicPage} />
            <Route exact path="/" component={PublicPage} />
            <Route exact path="/home" component={Home} />
            <Route exect path="/basket" component={Basket} />
            <Route exect path="/profile" component={Profile} />
            <Route exect path="/search" component={Search} />
          </BrowserRouter>
        </ProvideLoading>
      </ProvideAuth>
    </ApolloProvider>
  )
}

const EnhancedApp = withHelmet([
  { tag: 'title', content: 'Admin | Delivery-app' }
])(App)

export default EnhancedApp
