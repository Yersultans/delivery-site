import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Row, Col, Breadcrumb, Form, Input, Button, Result, Select } from 'antd'
import styled from 'styled-components'
import { MinusCircleOutlined, PlusCircleOutlined, UserOutlined, LockOutlined } from '@ant-design/icons'
import Visa from '../../img/svg/visa.svg'
import MasterCard from '../../img/svg/mastercard.svg'
import Cash from '../../img/svg/cash.svg'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import Loading from '../shared/Loading'

const BasketRow = styled(Row)`

`
const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`
const BasketContent = styled.div`
	padding: 16px;
  background-color: #f8f8f8;
	border-radius: 8px;
`

const BasketTitle = styled.div`
	font-weight: 500;
  font-size: 20px;
`
const Items = styled.div`
	margin-bottom: 16px;
	/* background-color: #fff; */
`

const Item = styled.div`
	background: #fff;
  padding: 16px;
  border-radius: 8px;
	display: flex;
`
const ItemImg = styled.img`
	width: 120px;
  height: 90px;
	border-radius: 8px;
`
const ItemTitle = styled.div`
	height: 56px;
`

const ItemInfo = styled.div`
	margin-left: 16px;
	width: -webkit-fill-available;
`

const ItemCount = styled.div`
	display: flex;
  align-items: center;
  justify-content: space-between;
`

const ItemPrice = styled.div`
	color: #FA4A0C;
`
const ShopProductQuantity = styled.div`
	display: flex;
	align-items: center;
	height: 32px;
`
const ShopProductNumber = styled.div`
		font-style: normal;
		font-weight: 600;
		font-size: 16px;
		line-height: 20px;
		color: #FA4A0C;
		margin: 0 8px;
`

const TotalContent = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 8px 0;
	border-top-style: dashed;
  border-top-width: 1px;
`

const TotalTitle = styled.div`
  font-weight: 500;
  margin-right: 8px;
`

const TotalPrice = styled.div`
	font-weight: 500;
  color: #FA4A0C;

`
const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
}

const StyledCol = styled(Col)`
	padding: 0 12px;
`
const CardIcon = styled.img`
	width: 32px;
  height: 32px;
	margin-right: 12px;
`


const Basket = ({ user, sum, items, addItem, deleteItem, setLoginModalVisible, userCards, handleAdd }) => {

	const handleCreateOrder = (valuse) => {
		handleAdd(valuse)
	}
  return (
		<>
			<StyledBreadcrumb>
					<Breadcrumb.Item>
						<Link to="/home">Home</Link>
					</Breadcrumb.Item>
					<Breadcrumb.Item>Checkout</Breadcrumb.Item>
				</StyledBreadcrumb>
			<BasketRow>
				<StyledCol span={12}>
				{user ? (
					<Form
					name="basic"
					{...layout}
					initialValues={{
						remember: true
					}}
					onFinish={handleCreateOrder}
				>
					<Form.Item 
						label="Address"
						rules={[
							{
								required: true,
								message: 'Please input your address!'
							}
						]}
					>
            <Input.Group compact>
              <Form.Item name={['address', 'street']} noStyle>
                <Input style={{ width: '100%', borderRadius: 0  }} placeholder="Street" />
              </Form.Item>
              <Form.Item name={['address', 'entrance']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Entrance" />
              </Form.Item>
              <Form.Item name={['address', 'apartment']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Apartment" />
              </Form.Item>
              <Form.Item name={['address', 'intercom']} noStyle>
                <Input style={{ width: '25%' }} placeholder="Intercom" />
              </Form.Item>
              <Form.Item name={['address', 'floor']} noStyle>
                <Input style={{ width: '25.7%', borderRadius: 0 }} placeholder="Floor" />
              </Form.Item>
            </Input.Group>
          </Form.Item>
					<Form.Item
            key="card"
            label="PaymentType"
            name="card"
            rules={[
              {
                required: true,
                message: `Selected Payment Type`
              }
            ]}
          >
            <Select mode="single" placeholder="Select a Payment Type" showSearch>
							<Select.Option
                key="cash"
                value="cash"
              >
								<CardIcon src={Cash} />
								Cash
              </Select.Option>
              {userCards &&
                userCards.map(userCard => (
                  <Select.Option
                    key={userCard?.id}
                    value={userCard?.identifier}
                  >
										<CardIcon src={userCard?.type === 'visa' ? Visa : MasterCard} />
										{userCard?.number.charAt(userCard?.number.length-4)}{userCard?.number.charAt(userCard?.number.length-3)}{userCard?.number.charAt(userCard?.number.length-2)}{userCard?.number.charAt(userCard?.number.length-1)}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
					<Form.Item {...tailLayout}>
						<Button type="primary" htmlType="submit" size="large">
							Сheckout
						</Button>
					</Form.Item>
				</Form>
				): (
					<Result
    				title="Please, login to continue"
    				extra={
      				<Button 
								type="primary" 
								key="console" 
								onClick={() => setLoginModalVisible(true)}
							>
        				login
      				</Button>
    				}
  				/>
				)}
					
				</StyledCol>
				<StyledCol span={12}>
					<BasketContent>
						<BasketTitle>Basket</BasketTitle>
						<Items>
							{items && items.map(item => (
								<Item>
									<ItemImg src={item?.imageUrl}/>
									<ItemInfo>
										<ItemTitle>{item?.name}</ItemTitle>
										<ItemCount>
											<ItemPrice>{item?.price*item?.quantity}₸</ItemPrice>
											<ShopProductQuantity>
												<MinusCircleOutlined onClick={() => 
												deleteItem(item.id, item?.shop?.id)
											} style={{
													color: '#FA4A0C'
												}}/>
												<ShopProductNumber>{item?.quantity}</ShopProductNumber>
												<PlusCircleOutlined 
												onClick={() => 
													addItem({
														...item,
														price: item?.price,
														shop: item?.shop
												})	
												}
												style={{
													color: '#FA4A0C'
												}}/>
											</ShopProductQuantity>
										</ItemCount>
									</ItemInfo>
									
								</Item>
							))}
						</Items>
						<TotalContent>
							<TotalTitle>Total:</TotalTitle>
							<TotalPrice>{sum}₸</TotalPrice>
						</TotalContent>
					</BasketContent>
				</StyledCol>
			</BasketRow>
		</>
	)
}

export default Basket
