import React from 'react'
import { gql, useMutation, useQuery, useLazyQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { Link, useHistory } from 'react-router-dom'

import { useAuth } from '../../context/useAuth'
import { useLoading } from '../../context/useLoading'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'
import Basket from './Basket'
import { removeAnnoyingHeader } from '../../utils/apollo'

const GET_CARDS = gql`
  query userCards($userId: ID) {
    userCards(userId: $userId) {
      id
      name
      expDate
      cvv
      type
      number
      amount
    }
  }
`

const ADD_ORDER = gql`
  mutation addOrder($input: OrderInput) {
    addOrder(input: $input) {
      id
    }
  }
`



const BasketContainer = ({ loginModalVisible, setLoginModalVisible }) => {
  const { user, sum, items, addItem, deleteItem, fetchUser, clearItems } = useAuth()
	const [userCards, setUserCards] = React.useState([])
	const history = useHistory()
	const [getUserCards, { data, loading, error, refetch }] = useLazyQuery(GET_CARDS, {
    fetchPolicy: 'no-cache'
  })
  const [addOrder] = useMutation(ADD_ORDER, {
    onCompleted() {
      clearItems()
			toast.success('Order is accepted')
			history.push('/profile')
    }
  })


	React.useEffect(() => {
		if(user){
			getUserCards({
        variables: {
          userId: user?.id
        }
      })
		}
	}, [user])

	React.useEffect(() => {
		if(data && !loading){
			setUserCards(data?.userCards)
		}
	}, [data, loading, error])

	const handleAdd = (values) => {
    let shops = []
    const renderItems = items.map(item => {
      if (!shops.includes(item.shop.id)) {
        shops = shops.concat([item.shop.id])
      }
      return {
        imageUrl: item.imageUrl,
        name: item.name,
        description: item.description,
        price: item.price,
        category: item.category,
        proteins: item.proteins,
        fats: item.fats,
        carbohydrates: item.carbohydrates,
        calories: item.calories,
        isVegan: item.isVegan,
        shop: item.shop.id,
        product: item.id,
        quantity: item.quantity,
        totalPrice: item.quantity * item.price
      }
    })
		console.log('asd', {
			items: renderItems,
			buyer: user.id,
			address: {
				apartment: values?.address?.apartment,
				comment: values?.address?.comment,
				entrance: values?.address?.entrance,
				floor: values?.address?.floor,
				intercom: values?.address?.intercom,
				street: values?.address?.street
			},
			paymentMethod: values?.card === 'cash' ? 'cash' : 'card',
			totalPrice: sum,
			shops,
			card:  values?.card === 'cash' ? null : values?.card
		})
		if (renderItems.length > 0) {
			addOrder({
				variables: {
					input: {
						items: renderItems,
						buyer: user.id,
						address: {
							apartment: values?.address?.apartment,
							comment: values?.address?.comment,
							entrance: values?.address?.entrance,
							floor: values?.address?.floor,
							intercom: values?.address?.intercom,
							street: values?.address?.street
						},
						paymentMethod: values?.card === 'cash' ? 'cash' : 'card',
						totalPrice: sum,
						shops,
						card:  values?.card === 'cash' ? null : values?.card
					}
				},
				errorPolicy: 'all'
			})
		}
	}

  return (
	<>
		<Basket 
			user={user}
			items={items} 
			addItem={addItem} 
			sum={sum} 
			deleteItem={deleteItem} 
			setLoginModalVisible={setLoginModalVisible}
			userCards={userCards}
			handleAdd={handleAdd}
		/>
	</>
	)
}

export default WithMainLayout(BasketContainer)
