import React from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

const GET_CURRENT_USER = gql`
  query getCurrentUser {
    getCurrentUser {
      id
      firstname
      lastname
      phoneNumber
      username
      role
      avatarUrl
    }
  }
`

const LOGOUT = gql`
  mutation logout {
    logout {
      message
    }
  }
`

const authContext = React.createContext()

// Provider hook that creates auth object and handles state
function useProvideAuth() {
  const [user, setUser] = React.useState(null)
  const [sendLogout] = useMutation(LOGOUT)
  const [items, setItems] = React.useState([])
  const [sum, setSum] = React.useState(0)


  const addItem = item => {
    console.log('')
    const index = items.findIndex(
      x => x.id === item.id && x.shop?.id === item.shop?.id
    )
    let newItem
    if (index < 0) {
      newItem = { ...item, quantity: 1 }
      const newArray = [...items, newItem]
      setItems(newArray)
      calculateSum(newArray)
    } else {
      const newArray = items
      newArray[index].quantity += 1
      setItems(newArray)
      calculateSum(newArray)
    }
    toast.success('Product added to basket')
  }
  const deleteItem = (id, shopId) => {
    const newArray = items
    const mapArray = newArray.map(item => {
      if (item.id === id && item.shop.id === shopId) {
        item.quantity -= 1
      }
      return item
    })
    const filterArray = mapArray.filter(item => item.quantity !== 0)
    setItems(filterArray)
    // calculate sum to update array
    calculateSum(filterArray)
    toast.success('Product removed from basket')
  }

  const clearItems = () => {
    const newArray = []
    setItems(newArray)
    // calculate sum to update array
    calculateSum(newArray)
    toast.success('Product removed from basket')
  }

  const calculateSum = array => {
    let number = 0
    array.forEach((item, index) => {
      number += item.price * item.quantity
      if (index === array.length - 1) setSum(number)
    })
    if (array.length === 0) setSum(0)
  }

  const {
    data,
    loading,
    error,
    refetch,
    networkStatus
  } = useQuery(GET_CURRENT_USER, { notifyOnNetworkStatusChange: true })

  React.useEffect(() => {
    if (!loading && data && data.getCurrentUser) {
      setUser(data.getCurrentUser)
    }
  }, [data, loading, error, networkStatus])

  function fetchUser() {
    refetch()
  }

  function logout() {
    sendLogout()
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    setUser(null)
  }

  function checkUserIsLoggedIn() {
    return localStorage.getItem('token') !== null
  }

  function getRole() {
    return localStorage.getItem('role')
  }

  // Return the user object and auth methods
  return {
    user,
    checkUserIsLoggedIn,
    fetchUser,
    logout,
    getRole,
    items,
    sum,
    setSum,
    addItem,
    deleteItem,
    clearItems
  }
}

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth({ children }) {
  const auth = useProvideAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
  return React.useContext(authContext)
}

ProvideAuth.propTypes = {
  children: PropTypes.node.isRequired
}
