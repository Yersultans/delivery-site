import React from 'react'
import PropTypes from 'prop-types'
import { Link, useHistory } from 'react-router-dom'
import styled from 'styled-components'
import {
  LogoutOutlined,
  TeamOutlined,
  RestOutlined,
  RadarChartOutlined,
  ShopOutlined,
  ShoppingOutlined
} from '@ant-design/icons'
import { Menu, Input } from 'antd'
import { useAuth } from '../context/useAuth'

import Loading from '../pages/shared/Loading'

const { Search } = Input

const LogoContainer = styled.div`
  box-sizing: border-box;
  background: #fff;
`

const StyledSearch = styled(Search)`
  padding: 32px;
`

const findGetParameter = params => {
	var result = null,
		tmp = []
	window.location.search
		.substr(1)
		.split('&')
		.forEach(item => {
			tmp = item.split('=')
			if (tmp[0] === params) result = decodeURIComponent(tmp[1])
		})
	return result
}
function MainMenu({ currentUrl }) {
  const { user, logout } = useAuth()
  const history = useHistory()
  const text = findGetParameter('text')
  const adminMenu = [
    { link: '/home', name: 'Home', icon: TeamOutlined }
  ]

  const userToMenu = {
    admin: adminMenu
  }

  const onLogoutClick = () => {
    logout()
    history.push('/login')
  }
  const onSearch = (text) => {
    history.push(`/search?text=${text}`)
  }
  
    return (
      <StyledSearch placeholder="input the name of the product or category" onSearch={onSearch} enterButton initalValue={text}/>
      // <Menu selectedKeys={[currentUrl]} mode="horizontal" >
      //   {adminMenu.map(item => (
      //     <Menu.Item key={item.link} >
      //       <Link to={item.link} key={item.link}>
      //         {item.name}
      //       </Link>
      //     </Menu.Item>
      //   ))}
      // </Menu>
    )
}

MainMenu.propTypes = {
  currentUrl: PropTypes.string.isRequired
}

export default MainMenu
