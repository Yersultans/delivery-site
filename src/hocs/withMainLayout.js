import React from 'react'
import styled from 'styled-components'
import { Layout, Button, Badge, Menu, Dropdown, Avatar, Row, Col } from 'antd'
import { gql, useMutation } from '@apollo/client'
import { toast } from 'react-toastify'
import { Link, useHistory } from 'react-router-dom'
import {
  ShoppingCartOutlined,
  UserOutlined,
  LogoutOutlined,
  OrderedListOutlined,
  GoogleOutlined,
  InstagramOutlined,
  FacebookOutlined,
  GitlabOutlined,
  GithubOutlined
} from '@ant-design/icons'

import MainMenu from './MainMenu'
import { useAuth } from '../context/useAuth'
import Logo from '../pages/logo.svg'
import { removeAnnoyingHeader } from '../utils/apollo'

import LoginModal from '../pages/home/LoginModal'

const { Header, Content, Sider, Footer } = Layout

const LogoContainer = styled.div`
  box-sizing: border-box;
  background: #fff;
  padding: 16px 0;
  cursor: pointer;
`


const StyledLayout = styled(Layout)`
  justify-content: center;
  display: flex;
  align-items: center;
  min-height: 100vh;
`

const StyledHeader = styled(Header)`
  min-height: 96px;
  display: flex;
  background-color: #fff;
  align-items: center;
  justify-content: space-between;
  width: 1240px;
`

const StyledImg = styled.img`
  width: 160px;
`

const UserContainer = styled.div`
  width: 120px;
`

const StyledContent = styled(Content)`
  background-color: #fff; 
  padding: 0 50px;
  width: 1240px;
`

const BacketContainer = styled.div`
  display: flex;
  padding: 12px;
  align-items: center;
  margin: 8px 16px;
  position: fixed;
  right: 0;
  bottom: 0;
  color: #fff;
  background-color: #FA4A0C;
  border-radius: 100px;
  cursor: pointer;
`

const ShopText = styled.div`
  font-size: 20px;
  margin-left: 8px;
`

const StyledFooter = styled(Footer)`
  min-height: 72px;
  display: flex;
  background-color: #000;
  color: #fff;
  width: 100%;
`

const StyledAvatar = styled(Avatar)`
`

const FooterContent = styled.div`
  width: 1240px;
  margin: auto;
  
`

const Icons = styled.div`
  text-align: center;
`

const FooterText = styled.div`
  margin: 24px 0;
  text-align: center;
`

const getItemLength = items => {
  let sumItem = 0
  items.forEach(item => {
    sumItem += item.quantity
  })
  return sumItem
}

const LOGIN = gql`
  mutation loginUser($input: LoginUserInput) {
    loginUser(input: $input) {
      token
    }
  }
`

const withMainLayout = Page => {
  return props => {
    const [login, { data: dataLogin, error: errorLogin, loading: loadingLogin }] = useMutation(LOGIN)
    const [loginModalVisible, setLoginModalVisible] = React.useState(false)
    const history = useHistory()
    const { user, fetchUser, logout, items } = useAuth()
    const currentUrl = history.location.pathname

    const [currentHeader, setCurrentHeader] = React.useState('')

    const adminMenu = [
      { link: '/users', name: 'Users' },
      { link: '/categories', name: 'Categories' },
      { link: '/products', name: 'Products' },
      { link: '/shops', name: 'Shops' }
    ]

    React.useEffect(() => {
      adminMenu.forEach(item => {
        if (currentUrl.startsWith(item.link)) {
          setCurrentHeader(item.name)
        }
      })
    }, [])

    React.useEffect(() => {
      if (!loadingLogin && errorLogin) {
        toast.error(removeAnnoyingHeader(errorLogin.message))
      } else if (dataLogin && dataLogin?.loginUser && dataLogin.loginUser?.token && !loadingLogin) {
        localStorage.setItem('token', `${dataLogin.loginUser?.token}`)
        toast.success('Successfully logged in')
        fetchUser()
      }
    }, [dataLogin, loadingLogin, errorLogin])
    

    const handleSubmit = values => {
      const { username, password } = values
      login({
        variables: {
          input: { username, password }
        },
        errorPolicy: 'all'
      })
    }
    
    const onLogoutClick = () => {
      logout()
      history.push('/home')
    }

    
    const menu = (
      <Menu>
        <Menu.Item
          key="profile"
          icon={<UserOutlined />}
        >
         <Link to='/profile' key='profile'>Profile</Link>
        </Menu.Item>
        <Menu.Item
          key="logout"
          icon={<LogoutOutlined />}
          onClick={onLogoutClick}
          danger
        >
          Sign Out
        </Menu.Item>
      </Menu>
    )


    return (
      <>
      <StyledLayout>
      <StyledHeader>
        <LogoContainer
          onClick={() => {
            history.push('/home')
          }}
        >
            <StyledImg src={Logo} alt="logo" />
        </LogoContainer>
        <MainMenu {...{ currentUrl, history, setCurrentHeader }} />
        <UserContainer>
        {user ? ( 
          <Dropdown overlay={menu}>
            {user?.avatarUrl ? (
              <StyledAvatar size={48} src={user?.avatarUrl} />
            ) : (
              <StyledAvatar size={48} icon={<UserOutlined />} />
            )}
          </Dropdown>
					) : (
						<Button 
              style={{
                marginLeft: 12,
                fontSize: 16
              }}
              onClick={() => {
                setLoginModalVisible(true)
							}}
            >
              Sign In
						</Button>
					)}
        </UserContainer>
      </StyledHeader>
      <StyledContent>
          <Page {...props} loginModalVisible={loginModalVisible} setLoginModalVisible={setLoginModalVisible}/>
      </StyledContent>
      <StyledFooter>
        <FooterContent>
          {/* <Row>
            <Col span={8}>
              <ColHeader></ColHeader>
            </Col>
            <Col span={8}>
              <ColHeader>Address</ColHeader>
            </Col>
            <Col span={8}><div>col-6</div></Col>
          </Row> */}
          <Icons>
            <GoogleOutlined style={{ fontSize: '36px', marginLeft: 16 }}/>
            <InstagramOutlined style={{ fontSize: '36px', marginLeft: 16 }}/>
            <FacebookOutlined  style={{ fontSize: '36px', marginLeft: 16 }}/>
            <GitlabOutlined  style={{ fontSize: '36px', marginLeft: 16 }}/>
            <GithubOutlined   style={{ fontSize: '36px', marginLeft: 16 }}/>
          </Icons>
          <FooterText>Delivery ©2022 Created by ASD</FooterText>
        </FooterContent>
      </StyledFooter>
    </StyledLayout>
    {loginModalVisible && (
      <LoginModal
        handleSubmit={handleSubmit}
        visible={loginModalVisible}
        setVisible={setLoginModalVisible}
    />
    )}
    <BacketContainer 
      onClick={() => {
        if(getItemLength(items) > 0){
          history.push('/basket')
        }
      
    }}>
      
        <ShoppingCartOutlined style={{
          fontSize: 24
        }}/>
        <ShopText>{getItemLength(items)}</ShopText>
    </BacketContainer>
    </>
    )
  }
}

export default withMainLayout
